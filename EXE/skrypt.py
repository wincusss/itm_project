import os
import shutil
from time import sleep
from collections import Counter

def getDataFromServer (data_path, enable_debug):
    dirSum=0
    ctr=0
    #get number of subdirectories
    if enable_debug:
        for subdir in os.listdir(data_path):
            if os.path.isdir(os.path.join(data_path, subdir)):
                dirSum+=1

    raw_array = []

    #iterate over subdirectories
    for subdir in os.listdir(data_path):
        subdir_data_path = data_path+subdir+'/'
        if os.path.isdir(subdir_data_path):
            if enable_debug:
                ctr+=1
                print('Data analysis progress: '+str(ctr)+'/'+str(dirSum))

            #iterate over files
            for filename in os.listdir(subdir_data_path):
                if os.path.isfile(subdir_data_path+filename):
                    if filename[-5:]=='F.png': #and filename[-5]=='A.lvm':
                        raw_array.append(filename)
    if enable_debug:
        print('Found '+str(len(raw_array))+' samples')

    raw_array.sort()

    #convert to list
    data = []
    for t in raw_array:
        data.append([t[1:3],t[3:5],t[5:7],t[8:10],t[10:12],t[12:14]])

    return data



data = getDataFromServer('/home/zmpsw/dane/', True)

#print(data)

#save data to a file
with open('/home/zmpsw/public_html/site/data_file.txt', 'w') as f:
    for t in data:
        f.write("%s\n" % t)

#print data
#for t in data:
#    print(t)
#    sleep(0.003)

#get years
#years = []
#for t in data:
#    years.append(t[0])


#calc cars in years
#a = dict(Counter(years))

#print('cars per year:')
#print(a)


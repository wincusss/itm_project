<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Opis zawartości strony">

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		
		<title>Strona główna</title>
		<link rel="stylesheet" href="style.css">		
	</head>
	<body>
    <div id="content_history">
        <style>
			* {box-sizing:border-box}
			
			/* Slideshow container */
			.slideshow-container {
			max-width: 900px;
			position: relative;
			margin: auto;
			}

			/* Frames */
			.frames {
			display: block;
  			margin-left: auto;
  			margin-right: auto;
			width: 640px;
			height: 480px;
			}

			/* Next & previous buttons */
			.prev, .next {
			cursor: pointer;
			position: absolute;
			top: 50%;
			width: auto;
			margin-top: -22px;
			padding: 16px;
			color: #bbb;
			font-size: 72px;
			transition: 0.6s ease;
			border-radius: 0 3px 3px 0;
			user-select: none;
			}

			/* Position the "next button" to the right */
			.next {
			right: 0;
			border-radius: 3px 0 0 3px;
			}

			/* On hover change color a bit */
			.prev:hover, .next:hover {
			color: rgba(0,0,0,0.8);
			}

			/* Caption text */
			.captiontext {
			color: white;
			padding: 8px 12px;
			position: absolute;
			bottom: 0px;
			width: 100%;
			font-size:14px;
			text-align: center;
			}

			/* Number text (1/3 etc) */
			.numbertext {
			color: white;
			padding: 8px 12px;
			position: absolute;
			top: -5;
			width: 100%;
			font-size:14px;
			text-align: center;
			}

			/* Fading animation */
			.fade {
			-webkit-animation-name: fade;
			-webkit-animation-duration: 1s;
            animation: forwards;
			animation-name: fade;
			animation-duration: 1s;
			}

			@-webkit-keyframes fade {
			from {opacity: .4}
			to {opacity: 1}
			}

			@keyframes fade {
			from {opacity: .4}
			to {opacity: 1}
			}
        </style>
        <h1 align="center">Wizualizacja danych z systemu pomiaru parametrów ruchu w formie graficznej.</h1>
        <br></br>
        <p style="text-align:center">Poniżej obraz z systemu pomiaru parametrów ruchu dla konkretego punktu w czasie (daty, godziny).</p>  
    
		<!-- Slideshow container -->
		<div class="slideshow-container">

			<!-- Full-width images with number and caption text -->
			<div class="mySlides fade">
				<div class="numbertext">1 / 1</div>
				<img class="frames" src="sub/default.png">
				<div class="captiontext">default.png</div>
			</div>
		
			<!-- Next and previous buttons -->
			<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			<a class="next" onclick="plusSlides(1)">&#10095;</a>
		</div>
		<br>
		
		<div style="text-align:center; margin-top:10px">
			<form name="hist_form" action="history.php" method="post" target="hist_iframe">
				<label for="date" style="margin-left: 15px; margin-right: 5px;">Dzień:</label>
				<input type="date" id="date" name="date">
				<label for="hour" style="margin-left: 15px">Godzina:</label>
				<input type="number" id="hour" name="hour" min="0" max="23" style="width: 60px">
				<button type="submit" name="ok" style="margin-left: 25px">OK</button>
			</form>
			<iframe id="hist_iframe" name="hist_iframe" style="display:none" onLoad="getFiles();"></iframe>
		</div>
	</div>
	<script type="text/javascript">
		var slideIndex = 1;
		var file_list = ["./sub/default.png"];

		showSlides(slideIndex);

		// Get file list from PHP script
		function getFiles() {
			var files = String(document.getElementById("hist_iframe").contentWindow.document.body.innerHTML);

			if (files) {
				file_list = files.split("\n");
				file_list = file_list.filter(function(e){return e});	// Remove all empty values
				console.log("Updated image paths:");
				console.log(file_list);
			}
			else {
				file_list = ["./sub/default.png"];
			}
			showSlides(slideIndex);
		}

		// Next/previous controls
		function plusSlides(n) {
			showSlides(slideIndex += n);
		}

		// Thumbnail image controls
		function currentSlide(n) {
			showSlides(slideIndex = n);
		}

		function showSlides(n) {
			var i;
			var slide = document.getElementsByClassName("mySlides")[0];
			var number = slide.getElementsByClassName("numbertext")[0];
			var caption = slide.getElementsByClassName("captiontext")[0];

			if (file_list.length == 0) {file_list = ["./sub/default.png"]}
			if (n > file_list.length) {slideIndex = 1}
			if (n < 1) {slideIndex = file_list.length}

			slide.children[1].src = file_list[slideIndex-1];
			console.log("New image src: ", slide.children[1].src);

			number.innerHTML = "".concat(String(slideIndex), "/", String(file_list.length));
			console.log("New image number: ", number.innerHTML);

			var dirnames = file_list[slideIndex-1].split("/");
			var newCaption = String(dirnames[dirnames.length - 1]);
			caption.innerHTML = (newCaption != '.') ? newCaption : caption.innerHTML;
			console.log("New image caption: ", caption.innerHTML);
		}
	</script>
	</body>
</html>

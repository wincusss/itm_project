<!DOCTYPE html>
<html>
<body>

<?php
    // Get all frames for given type and date
    $date =  $_POST['date'];
    $hour = $_POST['hour'];

    $date = $date ? $date : '*';
    $hour = $hour ? $hour : '*';

    $year = substr($date, 0, 4);
    $month = substr($date, 5, 2);
    $day = substr($date, 8, 2);
    if ($date) {
        $path = sprintf("./sub/dane/R%s_%s_%s/*", $year, $month, $day);
    }

    $year_short = substr($date, 2, 2);
    $files = glob($path);
    $pattern = sprintf("*/M%s%s%s_%s*_F.png", $year_short, $month, $day, $hour);
    foreach ($files as $filename) {
        if (fnmatch($pattern, $filename)) {
            echo "$filename" . "\n";
        }
    }
?>

</body>
</html>